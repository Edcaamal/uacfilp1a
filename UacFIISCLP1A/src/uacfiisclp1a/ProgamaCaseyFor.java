/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uacfiisclp1a;

import java.util.Scanner;

/**
 *
 * @author edgar
 */
public class ProgamaCaseyFor {

    /**
     * @param args the command line arguments
     */
    static void imprimirMensaje(String sMensaje){
        System.out.println(sMensaje);
    }
    
    static void separador(){
        imprimirMensaje("-----------------------------------------");
    }
    
    static void encabezado(){
        imprimirMensaje("Universidad Autónoma de Campeche");
        imprimirMensaje("Facultade de Ingeniería");
        imprimirMensaje("Ingeniería en Sistemas Computacionales");
        imprimirMensaje("Lenguajes de Programación 1 A");
        imprimirMensaje("EDCD");
        separador();
    }

    static void piepagina(){
        separador();
        imprimirMensaje("(c) UACAM-FI-ISC-LP1-A");
    }    
    
    static void sandwich(){
        separador();
        imprimirMensaje("Opción 1.- Sandwich");
        imprimirMensaje("  a.- Conseguir el Pan ");
        imprimirMensaje("  b.- Untales mayonesa ");
        imprimirMensaje("  c.- Ponerle Jamón  ");
        imprimirMensaje("  d.- Ponerle Queso ");
        imprimirMensaje("  e.- Calentar ");
    }    
    
    static void imprimirBarra(int porcentaje, String barra){
        System.out.print("["+porcentaje+"] ");
        System.out.println(barra);
    }

    static void barraIndividual(int cantidad){
        int valorbarra = (cantidad / 10);
        
        switch (valorbarra){
            case 0:
               imprimirBarra(cantidad, " " );
               break;
            case 1:
               imprimirBarra(cantidad,"==");
               imprimirBarra(cantidad,"==");
               break;
            case 2:
               imprimirBarra(cantidad,"====");
               imprimirBarra(cantidad,"====");
               imprimirBarra(cantidad,"====");
               imprimirBarra(cantidad,"====");
               break;
            case 3:
               imprimirBarra(cantidad,"======");
               break;
            case 4:
               imprimirBarra(cantidad,"========");
               break;
            case 5:
               imprimirBarra(cantidad,"==========");
               break;
            case 6:
               imprimirBarra(cantidad,"============");
               break;
            case 7:
               imprimirBarra(cantidad,"==============");
               break;
            case 8:
               imprimirBarra(cantidad,"================");
               break;
            case 9:
               imprimirBarra(cantidad,"==================");
               break;
            case 10:
               imprimirBarra(cantidad,"=====================");
               break;
            default:
               imprimirMensaje("Valor Incorrecto");
               break;
        }        
        
    }
    
    
    static void graficaBarras(){
        separador();
        barraIndividual(10);
        barraIndividual(20);
        barraIndividual(25);
        barraIndividual(28);
        barraIndividual(30);
        barraIndividual(15);
        barraIndividual(44);
        barraIndividual(70);
        barraIndividual(85);
        barraIndividual(90);
        barraIndividual(100);
        barraIndividual(90);
        
/*        Scanner entrada = new Scanner( System.in );
        imprimirMensaje("Teccle el valor a gáficar ");        
        int iOpcion = entrada.nextInt();
*/        
        
    }    
    
    
    static void submenu(int opcionmenu){
        switch (opcionmenu){
            case 1:
                sandwich();
                break;
            case 2:
                imprimirMensaje("Opción 2");
                imprimirMensaje("2.- Pescado ");
                break;
            case 3:
                imprimirMensaje("Opción 3");
                imprimirMensaje("3.- Carnes");
                break;
            case 4:
                imprimirMensaje("Opción 4");
                imprimirMensaje("4.- Aves");
                break;
            case 5:
               imprimirMensaje("Opción 5");
               imprimirMensaje("5.- Huevos");               
                break;
            case 6:
               graficaBarras();
                break;
            case 9:
               imprimirMensaje("Gracias por usar nuestra aplicación");
                break;
            default :
                imprimirMensaje("Opción Incorrecta ...");          
                break;
        }
    }
    
    static void menu(){
        imprimirMensaje("1.- Sandwich ");
        imprimirMensaje("2.- Pescado ");
        imprimirMensaje("3.- Carnes");
        imprimirMensaje("4.- Aves");
        imprimirMensaje("5.- Huevos");
        imprimirMensaje("6.- Gráfica de Barras");
        imprimirMensaje("9.- Salir");
        Scanner entrada = new Scanner( System.in );
        imprimirMensaje("Teccle la opción deseada __ ");        
        int iOpcion = entrada.nextInt();
        submenu(iOpcion);
    }
    
    
    public static void main(String[] args) {
        // TODO code application logic here
         encabezado();        
         menu();
         piepagina();                
   }
    
}

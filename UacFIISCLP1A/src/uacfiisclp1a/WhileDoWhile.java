/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uacfiisclp1a;

import java.util.Scanner;

/**
 *
 * @author edgar
 */
public class WhileDoWhile {

    public static void estructura_for(){
        
        for (int i= 1; i<=10; i ++){
            System.out.println("Valor de i "+i);
        }
 
        for (int i= 1; i<=10; i ++){
            System.out.println("Valor de i "+i);
        }

               
    }

        
    public static void estructura_while(){
        int x= 20;
        System.out.println("Ciclo While");
//        int x= 21;
        while (x<=20) {
            System.out.println("Valor de x "+x);
            x ++;
        }        while (x>=2) {
            System.out.println("Valor de x "+x);
            x -=2;
        }
        
        boolean valor = true;
        x = 1;
        while (valor){
            System.out.println("Valor de x "+x);
            x++;
            if (x == 25){
                valor = false;
            }   
        }
       
    }
    
 
    public static void estructura_doWhile(){
        System.out.println("Ciclo Do ... While");       
        int x= 1;
        do{
            System.out.println("Valor de x "+x);
            x ++;
        } while (x<=20);

        x= 21;
        do{
            System.out.println("Valor de x "+x);
            x --;
        } while (x>=2);
   
        System.out.println("Ciclo Do ... While Infinito ");       
        boolean valor = true;
        x = 1;
        do {
           System.out.println("Valor de x "+x);
           x++;
           if (x == 25){
                 valor = false;
            }   
        } while (valor);
        
    }
    
    public static void menuPrincipal(){
         System.out.println("1.- For ");
         System.out.println("2.- While ");
         System.out.println("3.- Do ... While ");
         System.out.println("9.- Salir ");        
    }
  
    public static void submenu(int opcion){
        switch (opcion){
            case 1:
                System.out.println("Esttucturas for ");
                estructura_for();
                break;
            case 2:
                System.out.println("Estrtucturas while ");
                estructura_while();
                break;
            case 3:
                System.out.println("Estructuras do ... while");
                estructura_doWhile();
                break;
            case 9:
                System.out.println("Gracias por usar nuestas aplicaciones ");
                break;
            default:
                System.out.println("Opcioón no valida ");
                break;
                        
        }
     }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       int opcionmenu;
       do {
         menuPrincipal();
         Scanner entrada = new Scanner(System.in);
         opcionmenu = entrada.nextInt(); 
         submenu(opcionmenu);
       } while (opcionmenu !=9 );
    }
    
}
